<!-- https://anilist.co/forum/thread/5558/comment/160484 -->
__Genre Challenge: Horror__
Challenge Start Date: 2019-04-25
Challenge Finish Date: YYYY-MM-DD
Legend: [X] = Completed [O] = Not Completed

<hr>

__Easy__

01) [X] __Watch a "Horror" anime with the genre “Action” or “Adventure”__
[Biohazard 4: Incubate](https://anilist.co/anime/8278)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

02) [X] __Watch a "Horror" anime with the genre “Comedy” or “Supernatural”__
[Corpse Princess: Kuro](https://anilist.co/anime/5034)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

03) [X] __Watch a "Horror" anime with the genre “Fantasy” or “Sci-Fi”__
[Tokyo Majin Gakuen Kenpucho: Tou](https://anilist.co/anime/1860)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

04) [X] __Watch a "Horror" anime with the genre “Psychological” or “Thriller”__
[Shiki Specials](https://anilist.co/anime/10083)
Start: 2020-11-28 Finish: 2020-11-28

05) [X] __Watch a "Horror" anime with the format “TV”__
[SHADOWS HOUSE 2nd Season](https://anilist.co/anime/139093)
Start: 2023-01-25 Finish: 2023-01-26

06) [X] __Watch a "Horror" anime with any format other than “TV”__
[Sankarea: Wagahai mo... Zombie de Aru...](https://anilist.co/anime/16694)
Start: 2023-04-07 Finish: 2023-04-07

07) [X] __Watch a "Horror" anime with a mean score of 70% or above on AniList__
[Ghost Hunt](https://anilist.co/anime/1571)
Start: YYYY-MM-DD Finish: YYYY-MM-DD // [Screenshot](https://files.everywan.ga/VUADciok.png)

08) [X] __Watch a "Horror" anime with a mean score of 69% or below on AniList__
[Supernatural The Animation](https://anilist.co/anime/8986)
Start: YYYY-MM-DD Finish: YYYY-MM-DD // [Screenshot](https://files.everywan.ga/kSDIu62E.png)

09) [X] __Watch a "Horror" anime with the source “Manga”__
[Dusk Maiden of Amnesia](https://anilist.co/anime/12445)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

10) [X] __Watch a "Horror" anime with any source other than “Manga”__
[Resident Evil: Degeneration](https://anilist.co/anime/3446)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

__Normal__

11) [X] __Watch a "Horror" anime that has a prequel or a sequel listed as a relation on AniList__
[AJIN: Demi-Human 2](https://anilist.co/anime/21799)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

12) [X] __Watch a "Horror" anime from different studios: 1st studio__
[Blood-C](https://anilist.co/anime/10490)
Start: 2020-11-29 Finish: 2020-11-29// Studio: [Production I.G](https://anilist.co/studio/10)

13) [X] __Watch a "Horror" anime from different studios: 2nd studio__
[Zetman](https://anilist.co/anime/11837)
Start: YYYY-MM-DD Finish: YYYY-MM-DD// Studio: [TMS Entertainment](https://anilist.co/studio/73)

14) [X] __Watch a "Horror" anime from different studios: 3rd studio__
[](https://anilist.co/anime/)
Start: YYYY-MM-DD Finish: YYYY-MM-DD// Studio: [](https://anilist.co/studio/)

15) [X] __Watch a "Horror" anime from different studios: 4th studio__
[Alien Nine](https://anilist.co/anime/1177)
Start: 2021-02-15 Finish: 2021-02-15// Studio: [J.C. Staff](https://anilist.co/studio/7)

16) [X] __Watch a "Horror" anime that started airing in 2004 or earlier__
[Hellsing](https://anilist.co/anime/270)
Start: 2022-08-27 Finish: 2022-08-28

17) [X] __Watch a "Horror" anime that started airing between 2005 and 2010__
[Tokko](https://anilist.co/anime/916)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

18) [X] __Watch a "Horror" anime that started airing between 2011 and the current year__
[Blood-C: The Last Dark](https://anilist.co/anime/10681)
Start: 2020-11-30 Finish: 2020-11-30

19) [X] __Watch a "Horror" anime with 12 episodes or less__
[Rin: Daughters of Mnemosyne](https://anilist.co/anime/3342)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

20) [X] __Watch a "Horror" anime with 13 episodes or more__
[The SoulTaker](https://anilist.co/anime/1048)
Start: 2023-03-22 Finish: 2023-03-23

__Hard__

21) [X] __Watch a "Horror" anime that started airing in any Spring or Summer season__
[Interlude](https://anilist.co/anime/1085)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

22) [X] __Watch a "Horror" anime that started airing in any Fall or Winter season__
[Highlander: The Search for Vengeance](https://anilist.co/anime/2178)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

23) [X] __Watch a "Horror" anime with a title that begins with a letter between A–L__
[Corpse Princess: Aka](https://anilist.co/anime/4581)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

24) [X] __Watch a "Horror" anime with a title that begins with a letter between M–Z__
[Neo Ranga](https://anilist.co/anime/1163)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

25) [X] __Watch a "Horror" anime that takes place in Japan__
[Kurozuka](https://anilist.co/anime/5039)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

26) [X] __Watch a “Horror” anime with a female main character__
[Shadow Star Narutaru](https://anilist.co/anime/838)
Start: YYYY-MM-DD Finish: YYYY-MM-DD// Character: [Shiina Tamai](https://anilist.co/character/2599)

27) [X] __Watch a “Horror” anime with any Horror Trope__
[Vampire Hunter D: Bloodlust](https://anilist.co/anime/543)
Start: YYYY-MM-DD Finish: YYYY-MM-DD// Trope: [Blood Bath](https://tvtropes.org/pmwiki/pmwiki.php/Main/BloodBath)

28) [X] __Watch a “Horror” anime that features a creature traditionally associated with the horror genre__
[Vampire Hunter D](https://anilist.co/anime/732)
Start: YYYY-MM-DD Finish: YYYY-MM-DD// Creature: Vampire

29) [X] __Watch a "Horror" anime from another participant’s challenge or a "Horror" anime recommended by a user on the AWC Discord__
[Blood: The Last Vampire](https://anilist.co/anime/405)
Start: YYYY-MM-DD Finish: YYYY-MM-DD // [Screenshot](https://files.everywan.ga/V5bsMZG5.png)

30) [X] __Watch a "Horror" anime you have not used for any other AWC Challenge__
[The Devil Lady](https://anilist.co/anime/1629)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

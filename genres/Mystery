<!-- https://anilist.co/forum/thread/5557/comment/161404 -->
__Genre Challenge: Mystery__
Challenge Start Date: 2019-04-28
Challenge Finish Date: YYYY-MM-DD
Legend: [X] = Completed [O] = Not Completed

<hr>

__Easy__

01) [X] __Watch a "Mystery" anime with the genre “Action” or “Adventure”__
[Special 7: Special Crime Investigation Unit](https://anilist.co/anime/108554)
Start: 2019-10-22 Finish: 2020-01-20

02) [X] __Watch a "Mystery" anime with the genre “Drama” or “Supernatural”__
[El Cazador de la Bruja](https://anilist.co/anime/2030)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

03) [X] __Watch a "Mystery" anime with the genre “Fantasy” or “Sci-Fi”__
[WONDER EGG PRIORITY: My Priority](https://anilist.co/anime/131773)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

04) [X] __Watch a "Mystery" anime with any of the genres: “Psychological”, “Thriller” or “Romance”__
[](https://anilist.co/anime/)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

05) [X] __Watch a "Mystery" anime with the format “TV”__
[Loveless](https://anilist.co/anime/149)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

06) [X] __Watch a "Mystery" anime with any format other than “TV”__
[Beyond the Boundary: Shinonome](https://anilist.co/anime/20779)
Start: 2020-09-14 Finish: 2020-09-14

07) [X] __Watch a "Mystery" anime with a mean score of 70% or above on AniList__
[Durarara!!](https://anilist.co/anime/6746)
Start: 2020-02-15 Finish: 2020-02-18 // [Screenshot](https://files.everywan.ga/SXLrTVkR.png)

08) [X] __Watch a "Mystery" anime with a mean score of 69% or below on AniList__
[Loups=Garous](https://anilist.co/anime/7598)
Start: YYYY-MM-DD Finish: YYYY-MM-DD // [Screenshot](https://files.everywan.ga/775uYlNR.png)

09) [X] __Watch a "Mystery" anime with the source “Manga”__
[Domain of Murder](https://anilist.co/anime/4756)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

10) [X] __Watch a "Mystery" anime with any source other than “Manga”__
[Durarara!! X2](https://anilist.co/anime/20652)
Start: 2021-04-10 Finish: 2021-04-10

__Normal__

11) [X] __Watch a "Mystery" anime that has a prequel or a sequel listed as a relation on AniList__
[Durarara!! X2 The Second Arc](https://anilist.co/anime/20879)
Start: 2021-08-23 Finish: 2021-08-24

12) [X] __Watch a "Mystery" anime from different studios: 1st studio__
[Durarara!! X2 The Third Arc](https://anilist.co/anime/20880)
Start: 2021-08-25 Finish: 2021-08-26// Studio: [Shuka](https://anilist.co/studio/6071)

13) [X] __Watch a "Mystery" anime from different studios: 2nd studio__
[Vampire Knight Guilty](https://anilist.co/anime/4752)
Start: YYYY-MM-DD Finish: YYYY-MM-DD// Studio: [Studio DEEN](https://anilist.co/studio/37)

14) [X] __Watch a "Mystery" anime from different studios: 3rd studio__
[Trickster](https://anilist.co/anime/21832)
Start: 2019-11-23 Finish: 2019-11-25// Studio: [TMS Entertainment](https://anilist.co/studio/73)

15) [X] __Watch a "Mystery" anime from different studios: 4th studio__
[The Big O](https://anilist.co/anime/567)
Start: YYYY-MM-DD Finish: YYYY-MM-DD// Studio: [Sunrise](https://anilist.co/studio/14)

16) [X] __Watch a "Mystery" anime that started airing in 2004 or earlier__
[Ghost in the Shell: Stand Alone Complex 2nd GIG](https://anilist.co/anime/801)
Start: 2021-05-29 Finish: 2021-05-31

17)  [X] __Watch a "Mystery" anime that started airing between 2005 and 2010__
[Ergo Proxy](https://anilist.co/anime/790)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

18) [X] __Watch a "Mystery" anime that started airing between 2011 and the current year__
[Joker Game](https://anilist.co/anime/21291)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

19) [X] __Watch a "Mystery" anime with 12 episodes or less__
[](https://anilist.co/anime/)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

20) [X] __Watch a "Mystery" anime with 13 episodes or more__
[Requiem from the Darkness](https://anilist.co/anime/279)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

__Hard__

21) [X] __Watch a "Mystery" anime that started airing in any Summer or Winter season​__
[Rampo Kitan: Game of Laplace](https://anilist.co/anime/21189)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

22) [X] __Watch a "Mystery" anime that started airing in any Spring or Fall season​__
[Darker than Black: Gemini of the Meteor](https://anilist.co/anime/6573)
Start: 2022-12-07 Finish: 2022-12-07

23) [X] __Watch a "Mystery" anime with a title that begins with a letter between A–L__
[Ghost in the Shell: Stand Alone Complex - Solid State Society](https://anilist.co/anime/1566)
Start: 2021-06-06 Finish: 2021-06-06

24) [X] __Watch a "Mystery" anime with a title that begins with a letter between M–Z__
[Persona 4 The Animation: No One is Alone](https://anilist.co/anime/13587)
Start: 2020-11-04 Finish: 2020-11-04

25) [X] __Watch a "Mystery" anime that takes place in a real-life location​__
[Red Garden](https://anilist.co/anime/1601)
Start: YYYY-MM-DD Finish: YYYY-MM-DD// Real Location: New York

26) [X] __Watch a “Mystery” anime with a main character that is an adult​__
[Occultic;Nine](https://anilist.co/anime/21708)
Start: YYYY-MM-DD Finish: YYYY-MM-DD// Character: [Sarai Hashinoue](https://anilist.co/character/85825)

27) [X] __Watch a “Mystery” anime with a main character that is not an adult​__
[Fantastic Children](https://anilist.co/anime/455)
Start: YYYY-MM-DD Finish: YYYY-MM-DD// Character: [Helga Lui](https://anilist.co/character/16679)

28) [X] __Watch a “Mystery” anime with a main character that has black, white, silver or grey hair​__
[](https://anilist.co/anime/)
Start: YYYY-MM-DD Finish: YYYY-MM-DD  // [](https://anilist.co/character/)// Hair Color: ???

29) [X] __Watch a "Mystery" anime from another participant’s challenge or a "Mystery" animerecommended by a user on the AWC Discord__
[Ghost Stories](https://anilist.co/anime/1281)
Start: YYYY-MM-DD Finish: YYYY-MM-DD // [Screenshot](https://files.everywan.ga/V5bsMZG5.png)

30) [X] __Watch a "Mystery" anime you have not used for any other AWC Challenge__
[CLAMP School Detectives](https://anilist.co/anime/1844)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

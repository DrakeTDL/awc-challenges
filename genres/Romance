<!-- https://anilist.co/forum/thread/5291/comment/161582 -->
__Genre Challenge: Romance__
Challenge Start Date: 2019-04-29
Challenge Finish Date: YYYY-MM-DD
Legend: [X] = Completed [O] = Not Completed

<hr>

__Easy__

01) [X] __Watch a “Romance” anime with the genre “Action” or “Adventure”__
[Ceres, Celestial Legend](https://anilist.co/anime/104)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

02) [X] __Watch an anime with the genres “Romance” and “Comedy”__
[Science Fell in Love, So I Tried to Prove It](https://anilist.co/anime/107067)
Start: 2020-02-26 Finish: 2020-05-10

03) [X] __Watch a “Romance” anime with the tag “Shounen” or “Seinen”__
[Hi Score Girl II](https://anilist.co/anime/108581)
Start: 2020-08-03 Finish: 2020-08-03

04) [X] __Watch a “Romance” anime with the tag “Shoujo” or “Josei”__
[Pretty Guardian Sailor Moon Eternal The Movie Part 2](https://anilist.co/anime/103712)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

05) [X] __Watch a “Romance” anime with the genre “Drama” or “Slice of Life”__
[Rent-a-Girlfriend](https://anilist.co/anime/113813)
Start: 2020-09-14 Finish: 2020-11-13

06) [X] __Watch a “Romance” anime with the genre “Fantasy” or “Sci-Fi”__
[The Testament of Sister New Devil](https://anilist.co/anime/20678)
Start: 2021-09-04 Finish: 2021-09-05

07) [X] __Watch a “Romance” anime with any of the tags: “Boys' Love”, “Yuri” or “Bisexual”__
[Bloom Into You](https://anilist.co/anime/101573)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

08) [X] __Watch a “Romance” anime with any of the genres: “Mystery”, “Psychological”, “Horror”,“Thriller” or “Supernatural”__
[Hatena Illusion](https://anilist.co/anime/98515)
Start: 2020-01-24 Finish: 2020-09-03

09) [X] __Watch a "Romance" anime with the format “Special”__
[DearS Special](https://anilist.co/anime/1817)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

10) [X] __Watch a "Romance" anime with the format “OVA” or “ONA”__
[The World God Only Knows: 4 Girls and an Idol](https://anilist.co/anime/10805)
Start: 2019-12-18 Finish: 2019-12-18

11) [X] __Watch a "Romance" anime with the format “Movie”​_​_
[Love, Chunibyo & Other Delusions: Take on Me](https://anilist.co/anime/98762)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

12) [X] __Watch a "Romance" anime with the format “TV”__
[The Fruit of Evolution: Before I Knew It, My Life Had It Made Season 2](https://anilist.co/anime/146954)
Start: 2023-01-27 Finish: 2023-04-17

13) [X] __Watch a "Romance" anime with the format “TV” or “TV Short”__
[Ladies Versus Butlers](https://anilist.co/anime/7148)
Start: 2021-06-01 Finish: 2021-06-01

14) [X] __Watch a “Romance” anime with a mean score of 80% or above on AniList__
[Tsukigakirei](https://anilist.co/anime/98202)
Start: YYYY-MM-DD Finish: YYYY-MM-DD // [Screenshot](https://files.everywan.ga/sby5uP1O.png)

15) [X] __Watch a “Romance” anime with a mean score of 70% to 79% on AniList__
[To Love Ru Darkness](https://anilist.co/anime/13663)
Start: 2023-05-05 Finish: 2023-05-05 // [Screenshot](https://files.everywan.ga/03dme6.png)

__Normal__

16) [X] __Watch a “Romance” anime with a mean score of 69% or below on AniList__
[The Testament of Sister New Devil BURST](https://anilist.co/anime/21110)
Start: 2022-07-23 Finish: 2022-07-23 // [Screenshot](https://files.everywan.ga/pyRq4jxL.png)

17) [X] __Watch a “Romance” anime with the source “Original”__
[Hand Maid May](https://anilist.co/anime/318)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

18) [X] __Watch a “Romance” anime with the source “Novel”, “Light Novel” or “Visual Novel”__
[Koikishi Purely☆Kiss The Animation](https://anilist.co/anime/18655)
Start: 2020-11-10 Finish: 2020-11-10

19) [X] __Watch a “Romance” anime with the source “Manga”__
[DearS](https://anilist.co/anime/63)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

20) [X] __Watch a "Romance" anime from different studios: 1st studio__
[Hitorijime My Hero](https://anilist.co/anime/87494)
Start: YYYY-MM-DD Finish: YYYY-MM-DD// Studio: [Encourage Films](https://anilist.co/studio/354)

21) [X] __Watch a "Romance" anime from different studios: 2nd studio__
[Liar Liar](https://anilist.co/anime/131863)
Start: YYYY-MM-DD Finish: YYYY-MM-DD// Studio: [GEEKTOYS](https://anilist.co/studio/291)

22) [X] __Watch a "Romance" anime from different studios: 3rd studio__
[Eden of The East the Movie I: The King of Eden](https://anilist.co/anime/6372)
Start: YYYY-MM-DD Finish: YYYY-MM-DD// Studio: [Production IG](https://anilist.co/studio/10)

23) [X] __Watch a "Romance" anime from different studios: 4th studio__
[Love Tyrant](https://anilist.co/anime/21517)
Start: YYYY-MM-DD Finish: YYYY-MM-DD// Studio: [EMT Squared](https://anilist.co/studio/6125)

24) [X] __Watch a "Romance" anime that started airing in 1999 or earlier__
[To Heart](https://anilist.co/anime/472)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

25) [X] __Watch a "Romance" anime that started airing between 2000 and 2004__
[Nanaka 6/17](https://anilist.co/anime/776)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

26) [X] __Watch a "Romance" anime that started airing between 2005 and 2009__
[Skip Beat!](https://anilist.co/anime/4722)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

27) [X] __Watch a "Romance" anime that started airing between 2010 and 2014__
[](https://anilist.co/anime/)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

28) [X] __Watch a "Romance" anime that started airing between 2015 and the current year__
[I Got a Cheat Skill in Another World and Became Unrivaled in The Real World, Too](https://anilist.co/anime/153845)
Start: 2023-05-16 Finish: YYYY-MM-DD

29) [X] __Watch a "Romance" anime with 12 episodes or less__
[The Aristocrat’s Otherworldly Adventure: Serving Gods Who Go Too Far](https://anilist.co/anime/153332)
Start: 2023-04-24 Finish: YYYY-MM-DD

30) [X] __Watch a "Romance" anime with 24 episodes or more__
[](https://anilist.co/anime/)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

__Hard__

31) [X] __Watch a "Romance" anime that started airing in any Spring season__
[Banner of the Stars](https://anilist.co/anime/396)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

32) [X] __Watch a "Romance" anime that started airing in any Summer season__
[ReLIFE](https://anilist.co/anime/21049)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

33) [X] __Watch a "Romance" anime that started airing in any Fall season__
[](https://anilist.co/anime/576)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

34) [X] __Watch a "Romance" anime that started airing in any Winter season__
[Magikano](https://anilist.co/anime/20916)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

35) [X] __Watch a "Romance" anime with a title that begins with a letter between A–L__
[Banner of the Stars II](https://anilist.co/anime/397)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

36) [X] __Watch a "Romance" anime with a title that begins with a letter between M–Z__
[The Ambition of Oda Nobuna](https://anilist.co/anime/11933)
Start: 2022-09-15 Finish: 2022-09-15

37) [X] __Watch a “Romance” anime with four or more words in the title__
[Maria the Virgin Witch](https://anilist.co/anime/20840)
Start: 2022-08-18 Finish: 2022-08-18

38) [X] __Watch a “Romance” anime that takes place in a school setting__
[Convenience Store Boy Friends](https://anilist.co/anime/98631)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

39) [X] __Watch a “Romance” anime that features a type of “dere” character__
[Nyan Koi!](https://anilist.co/anime/6512)
Start: YYYY-MM-DD Finish: YYYY-MM-DD // Character: [Akari Kirishima](https://anilist.co/character/27082)_// Type: [Tsundere](https://en.wikipedia.org/wiki/NyanKoi!#Akari_Kirishima)_

40) [X] __Watch a “Romance” anime with a shy or unpopular character that likes a popularcharacter__
[Say "I Love You".](https://anilist.co/anime/14289)
Start: YYYY-MM-DD Finish: YYYY-MM-DD// Shy/Unpopular Character: [Mei Tachibana](https://anilist.co/character/24201)// Popular Character: [Yamato Kurosawa](https://anilist.co/character/26243)

41) [X] __Watch a “Romance” anime with a main character that has a harem or reverse harem__
[Ai Yori Aoshi](https://anilist.co/anime/53)
Start: YYYY-MM-DD Finish: YYYY-MM-DD// Character: [Kaoru Hanabishi](https://anilist.co/character/5892)

42) [X] __Watch a “Romance” anime that features a love triangle__
[Mars Daybreak](https://anilist.co/anime/1086)
Start: YYYY-MM-DD Finish: YYYY-MM-DD

43) [X] __Watch a “Romance” anime with any Love Interest Trope__
[Chrono Crusade](https://anilist.co/anime/60)
Start: 2023-04-26 Finish: 2023-05-26// Trope: [Naughty Nun](https://tvtropes.org/pmwiki/pmwiki.php/Main/NaughtyNuns?from=Main.NaughtyNun)

44) [X] __Watch a "Romance" anime from another participant’s challenge or a "Romance" animerecommended by a user on the AWC Discord__
[Gosick](https://anilist.co/anime/8425)
Start: YYYY-MM-DD Finish: YYYY-MM-DD // [Screenshot](https://files.everywan.ga/V5bsMZG5.png)

45) [X] __Watch a "Romance" anime you have not used for any other AWC Challenge__
[Shakugan no Shana S](https://anilist.co/anime/6572)
Start: 2023-04-07 Finish: 2023-04-07
